# CellularBox

## Details
<p>This tool implements the visualization of one-dimensional cellular automata (https://en.wikipedia.org/wiki/Elementary_cellular_automaton)</p>
<p>It's mostly just a fun project to practice common sdl/c programming concepts.</p>

![Alt text](screenshotCB.png "CellularBox")
![Alt text](screenshotCB2.png "CellularBox")

<p>So far you can preview the first 540 generations and zoom.</p>
Possible features are:
<ul>
    <li>Choose one of the 256 possible rules (done)</li>
    <li>Random initial conditions</li>
    <li>Adjust grow speed</li>
    <li>Jump to a given row</li>
    <li>Scrolling</li>
    <li>Edit Colors</li>
</ul>


## Build

<br>

**Linux:** 
<li>Download <a href="https://github.com/libsdl-org/SDL/releases">sdl2</a></li>
<li>Download <a href="https://github.com/libsdl-org/SDL_ttf/releases">sdl_ttf</a></li>
<li>Extract each archive and run
<ol> 
    <li>"./configure</li>
    <li>make</li>
    <li>make install</li>
</ol>one after another for both, perhaps start with sdl2 itself.
</li>
<li>run ./buildCellularBox.sh</li>
<br>

**Windows:**

<li>Download the file "...mingw.zip." 
<a href="https://github.com/libsdl-org/SDL/releases">sdl2</a></li>
<li>Download the file "...mingw.zip." <a href="https://github.com/libsdl-org/SDL_ttf/releases">sdl_ttf</a></li>
<li>Extract both, merge the include folders and lib folders of both in a place you want to link to.</li>
<li>edit & run buildCellularBox.bat.</li>
<br>
If anyone struggles to build for linux or windows, tell me I will upload one quickly.

## Run

**Linux:**
<li>The font folder in the repo is expected to be in one folder with the build file.</li>
<br>

**Windows:** 
<li>The font folder in the repo is expected to be in one folder with the build file.</li>
<li>sdl2.dll and sdl_ttf.dll is expected to be in one folder with the build file, those are found in the "bin" folders of the archives you downloaded.</li>
