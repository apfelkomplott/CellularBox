#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <array>
#include <iostream>

// Screen dimension
const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

// Holds the frames per second
int iFps = 0;
// Holds the time passed since last frame
Uint64 iTimePassed = 0;

// Number of rects to be filled onto the surface
int iCountRectsRendered = 0;

// Current boundary for printed generations
const int iGenerationMax = 540;
// Total gen numbers to be generated
const int iGenCount = iGenerationMax / 10;
// Total numbers of row numbers to be printed in the view
const int iRowNumbersMax = 11;
// Cell starting width, changed by zoom functions
int iCellWidth = 2;
// The number of generations to be printed, changed by zoom funtions
int iGenStop = 540;


// Pointers Visuals
SDL_Window *pWindow = nullptr;
SDL_Renderer *pRenderer = nullptr;
TTF_Font *pFont = nullptr;
SDL_Surface *pSurfaceText = nullptr;
// Surface to draw the rules to
SDL_Surface *pSurfaceMap = nullptr;
// Texture of the rule surface
SDL_Texture *pTextureMap = nullptr;

struct sTextBox {
    SDL_Rect rText;        // Place of the textbox, width and height depend on the given font size, so these values are calculated in SetTextBoxDim(). An object of type SDL_Rect can be intialized like this: rect = {x, y, width, height}
    std::string sText;     // Content
    int iFontSize;         // Fontsize
    SDL_Texture *pTexture; // Pointer to access the texture of the box
};

sTextBox sText_title{
    (SCREEN_WIDTH / 2) - 215, (SCREEN_HEIGHT / 2) + 170, 0, 0,
    "CellularBox",
    80,
    nullptr};

sTextBox sText_start{
    (SCREEN_WIDTH / 2) - 215, (SCREEN_HEIGHT / 2) + 350, 0, 0,
    "Press space to start",
    50,
    nullptr};

sTextBox sText_Keys{
    1500, 30, 0, 0,
    "Use <- -> to preview",
    30,
    nullptr};

sTextBox sText_Zoom{
    1500, 100, 0, 0,
    "+ - to zoom",
    30,
    nullptr};

sTextBox sText_Rule{
    120, 30, 0, 0,
    "Rule: ",
    40,
    nullptr};

sTextBox sText_RuleValue{
    223, 30, 0, 0,
    "0",
    40,
    nullptr};

sTextBox sText_CellSize{
    120, 80, 0, 0,
    "Cellsize in px: ",
    40,
    nullptr};

sTextBox sText_CellSizeValue{
    379, 80, 0, 0,
    "2",
    40,
    nullptr};

sTextBox sText_Fps{
    0, 0, 0, 0,
    "0",
    20,
    nullptr};

// Array of text-boxes to store all gen numbers
std::array<sTextBox, iGenCount> sText_GenNum;
/*
Array of text-boxes pointers to point to only those gen numbers that are being rendered (depends on the zooming level)
The idea is to have one array that points to all textboxes, holding the row numbers sText_GenNum[0...iGenCount] (10, 20, 30 ..., iGenerationMax)
And a second array of pointers psText_GenNum, only pointing to those textboxes that I want to render for a given zoom scale since it looks bad when for a max zommed out view, every 10th row a row numbers is squeezed in the picture.
*/
// Array of pointers, containing the addresses of only those row number textures that are suppoed to be rendered for the current zoom scale
std::array<sTextBox *, iRowNumbersMax> psText_GenNum;
// Font size of the gen numbers
const int iGenNumSize = 15;

// Array of text-box pointers to simplify setup
std::array<sTextBox *, 9> sBoxes = {
    &sText_title,
    &sText_start,
    &sText_Keys,
    &sText_Rule,
    &sText_RuleValue,
    &sText_Zoom,
    &sText_CellSize,
    &sText_CellSizeValue,
    &sText_Fps};


// Cell colours

// Dark yellow/Arylide yellow
const SDL_Colour cDarkYellow = {233, 214, 107, 255};
// Black
const SDL_Colour cBlack = {0, 0, 0, 255};
// Robin egg blue
const SDL_Colour cEggBlue = {0, 204, 204, 255};
// Alpha animation, this is used to animated blinking "Press space to start" text
int iAlphaAn = 0;
bool bDecreaseAlpha = false;
/*

One dimensional automata with 2 possible states are described by a ruleset,
which decides the states for the succeeding generation based on the current cell and its 2 neighbours.
That makes 2³ = 8 different cases to be described for one rulset.
There are 2^8 = 256 possible rules


 7     6     5     4     3     2     1     0
111   110   101   100   011   010   001   000
 #     #     #     #     #     #     #     #

*/

struct sConfig {
    const bool bleft;
    const bool bmiddle;
    const bool bright;
    bool bset;
};

std::array<sConfig, 8> bConfigurationMap = {
    0, 0, 0, 0,
    0, 0, 1, 0,
    0, 1, 0, 0,
    0, 1, 1, 0,
    1, 0, 0, 0,
    1, 0, 1, 0,
    1, 1, 0, 0,
    1, 1, 1, 0};

int chInputNumber;
char chInputNumberBin[8];

struct sCell {
    SDL_Rect rCell;
    SDL_Color cCell;
    bool bActive;
};

// The current generation
int iGeneration = 1;
// The current generations cells
// iCell counts cells in a row
int iCell = 1;
// iTotalCells holds the number of all cells in the current view
int iCountTotalCells = 1;
// Calculate the array size that is need for a give amount of generations
int GetTotalCells(int);
// The size of the array determined by the given number of total generation iGenStop
const int iTotalCells = GetTotalCells(iGenerationMax);
// The upper limit cell view to be rendered, changed by zoom functions
int iZoomedCells = iTotalCells;
// The whole data
sCell *sCellMap = new sCell[iTotalCells];
// Array of SDL_Rects
SDL_Rect *sCellRects = new SDL_Rect[iTotalCells];

/* End the CellularBox state 1 or 2
Instead of checking the current state of the game in each frame and using only one loop with the same functions
I rather use 2 different functions for that to have a more efficient program
*/
bool bEndBox_Start = false;
bool bEndBox_Main = false;

// Print linked and compiled sdl versions at program start
void CheckUsedVersions();
void AdjustTextNumberRow(int);
// Refreshs number values printed on screen like the rule number and cell size
void SetupTextNumber(sTextBox *, int);
/*
This is needed to have consistent sized font rendering,
as width and height of a textBox depend on a string's size and font size
*/
void SetTextBoxDim(TTF_Font *, sTextBox *);
bool SetupText(sTextBox *);
bool SetupTextAll();
bool InitBox();
void PrintFps();
void SetupStartScreen();
void SetInput();
// Handles key input for the starting screen loop
void HandleKeys_S(SDL_Scancode *);
// Handles key input for the main program loop
void HandleKeys_M(SDL_Scancode *);
void InitRuleset(int);
void ClearNewFrame();
void ClearRule();
void FlushRects();
// Draws the start screen
void DrawCurrentFrame_S();
/*
Draws the main program
No calculations or expensive calls are supposed to be happening here,
just anything that was prepared in advance is being served to this function
to be rendered at last
*/
void DrawCurrentFrame_M();
void SetupNextGeneration(int);
void SetupActiveMap();
bool SetupMapTexture();
bool DecideCell(sCell *, int *, int *);
void ZoomCellsIn();
void ZoomCellsOut();
void ZoomCells(int);
void QuitBox();

int main(int argc, char *args[])
{
    CheckUsedVersions();

    // Mouse used
    bool bUsedMouse = false;
    if (!InitBox()) {
        QuitBox();
        return 0;
    }
    SetupStartScreen();
    // Handle Input
    SDL_Event e;
    // Mouse
    int mx, my;

    while (bEndBox_Start == false) {
        ClearNewFrame();

        // Handle Input if nothing is being animated
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                bEndBox_Start = true;
                bEndBox_Main = true;
            }
            if (e.type == SDL_MOUSEMOTION) {
                bUsedMouse = false; // Only used for debugging
            }
            if (e.type == SDL_KEYDOWN) {
                HandleKeys_S(&e.key.keysym.scancode);
            }
        }
        if (bUsedMouse) {
            SDL_GetMouseState(&mx, &my);
            std::cout << "x,y: " << mx << "," << my << std::endl;
        }
        bUsedMouse = false;

        DrawCurrentFrame_S();
        PrintFps();
    }
    chInputNumber = 0;
    InitRuleset(chInputNumber);
    ClearRule();
    for (int i = 1; i < iGenerationMax; i++) {
        SetupNextGeneration(iCellWidth);
    }
    SetupActiveMap();
    SetupMapTexture();
    while (bEndBox_Main == false) {
        ClearNewFrame();
        // Handle Input if nothing is being animated
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                bEndBox_Main = true;
            }
            if (e.type == SDL_MOUSEMOTION) {
                bUsedMouse = false; // Only set to true for debugging
            }
            if (e.type == SDL_KEYDOWN) {
                HandleKeys_M(&e.key.keysym.scancode);
            }
        }
        DrawCurrentFrame_M();
        PrintFps();
    }
    QuitBox();
    return 0;
}

void CheckUsedVersions()
{
    // Make sure the right sdl versions are being used to link and compile
    SDL_version vcompiled;
    SDL_version vlinked;
    SDL_VERSION(&vcompiled);
    SDL_GetVersion(&vlinked);
    // TTF
    SDL_version vcompiled_ttf;
    const SDL_version *vlinked_ttf = TTF_Linked_Version();
    SDL_TTF_VERSION(&vcompiled_ttf);

    std::cout << "Compiled against SDL version: " << (int)vcompiled.major << "." << (int)vcompiled.minor << "." << (int)vcompiled.patch << std::endl;
    std::cout << "Linked against SDL version: " << (int)vlinked.major << "." << (int)vlinked.minor << "." << (int)vlinked.patch << std::endl;

    std::cout << "Compiled against SDL_ttf version: " << (int)vcompiled_ttf.major << "." << (int)vcompiled_ttf.minor << "." << (int)vcompiled_ttf.patch << std::endl;
    std::cout << "Linked against SDL_ttf version: " << (int)vlinked_ttf->major << "." << (int)vlinked_ttf->minor << "." << (int)vlinked_ttf->patch << std::endl;

    std::cout << "C++ Version: " << __cplusplus << std::endl;
}
int GetTotalCells(int iGenTotal)
{
    int isumrow = 0, isumtotal = 0;
    for (int i = 0; i < iGenTotal; i++) {
        isumrow = isumrow + 2;
        isumtotal += isumrow;
    }
    return (isumtotal - iGenTotal);
}
void SetTextBoxDim(TTF_Font *font, sTextBox *box)
{
    TTF_SizeUTF8(font, box->sText.c_str(), &box->rText.w, &box->rText.h);
    // std::cout << "Needed width and height for " << box->sText << " are h;w: " << box->rText.h << ";" << box->rText.w << std::endl;
}
bool SetupText(sTextBox *box)
{
    SDL_Texture *pTex;
    // Load fonts
    pFont = TTF_OpenFont("font/LinBiolinum_RB.ttf", box->iFontSize);
    if (pFont == nullptr) {
        std::cout << "TTF_OpenFont Error: " << TTF_GetError() << std::endl;
        return false;
    }
    SetTextBoxDim(pFont, box);

    pSurfaceText = TTF_RenderUTF8_Blended(pFont, box->sText.c_str(), cEggBlue);
    if (pSurfaceText == nullptr) {
        std::cout << "TTF_RenderUTF8_Solid Error: " << TTF_GetError() << std::endl;
        return false;
    }
    pTex = SDL_CreateTextureFromSurface(pRenderer, pSurfaceText);
    if (pTex == nullptr) {
        std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
        return false;
    }
    SDL_FreeSurface(pSurfaceText);
    TTF_CloseFont(pFont);
    pFont = nullptr;
    pSurfaceText = nullptr;
    box->pTexture = pTex;
    return true;
}
void AdjustTextNumberRow(int iWidth)
{
    // Align row numbers to resized cell rows
    for (int i = 0; i < iGenCount; i++) {
        sText_GenNum[i].rText.x = ((SCREEN_WIDTH / 2) - ((i + 1) * (10 * iWidth)) - 50);
        sText_GenNum[i].rText.y = (((i + 1) * (10 * iWidth)) - 5);
    }
    /* Depending on the zooming level, only specific rows numbers are shown
    For px/width
    10: 10
    9: 10
    8: 11
    7: every second: 20; 50; 70 ...
    6: every second: 20; 50; 70 ...
    5: every second: 20; 50; 70 ...
    4: every third: 30; 60; 90 ...
    3: every third: 30; 60; 90 ...
    2: every fourth: 40; 80; 120 ...
    */
    if (iWidth > 7) {
        for (int i = 0; i < iRowNumbersMax; i++) {
            psText_GenNum.at(i) = &sText_GenNum.at(i);
            // std::cout << "psText_GenNum[" << i << "]->sText :" << psText_GenNum[i]->sText << std::endl;
        }
        // [1] = 20; [3] = 40; [5] = 60
    } else if (iWidth > 4) {
        for (int i = 0; i < iRowNumbersMax; i++) {
            psText_GenNum.at(i) = &sText_GenNum.at((i * 2) + 1);
            // std::cout << "psText_GenNum[" << i << "]->sText :" << psText_GenNum[i]->sText << std::endl;
        }
        // [2] = 30; [5] = 60; [8] = 90; [11] = 120;
    } else if (iWidth > 2) {
        for (int i = 0; i < iRowNumbersMax; i++) {
            psText_GenNum.at(i) = &sText_GenNum.at((i * 3) + 2);
            // std::cout << "psText_GenNum[" << i << "]->sText :" << psText_GenNum[i]->sText << std::endl;
        }
        // [3] = 40; [7] = 80; [11] = 120;
    } else {
        for (int i = 0; i < iRowNumbersMax; i++) {
            psText_GenNum.at(i) = &sText_GenNum.at((i * 4) + 3);
            // std::cout << "psText_GenNum[" << i << "]->sText :" << psText_GenNum[i]->sText << std::endl;
        }
    }
}
void SetupTextNumber(sTextBox *box, int iNumber)
{
    SDL_DestroyTexture(box->pTexture);
    box->sText = std::to_string(iNumber);
    SetupText(box);
}
bool SetupTextAll()
{
    for (int i = 0; i < (int)sBoxes.size(); i++) {
        if (!SetupText(sBoxes.at(i))) {
            return false;
        }
    }
    // Row numbers initiate
    for (int i = 0; i < iGenCount; i++) {
        sText_GenNum[i].sText = std::to_string((i + 1) * 10);
        sText_GenNum[i].iFontSize = iGenNumSize;
        if (!SetupText(&sText_GenNum.at(i))) {
            return false;
        }
    }
    AdjustTextNumberRow(iCellWidth);
    return true;
}
bool InitBox()
{
    // Setup video
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Get informations about the current display
    SDL_DisplayMode displayMode;
    SDL_GetDesktopDisplayMode(0, &displayMode);
    std::cout << "DisplayMode -> "
              << "Width: " << displayMode.w << "; Height: " << displayMode.h << "; Refresh rate: " << displayMode.refresh_rate << std::endl;

    // Create a window
    pWindow = SDL_CreateWindow("CellularBox", SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                               SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (pWindow == NULL) {
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Create a renderer, use VSYNC
    pRenderer = SDL_CreateRenderer(pWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (pRenderer == NULL) {
        std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        return false;
    }

    // Initialize font lib
    if (TTF_Init() != 0) {
        std::cout << "TTF_Init Error: " << TTF_GetError() << std::endl;
        return false;
    }

    // Initialize all Textures
    if (SetupTextAll() != true) {
        return false;
    }

    pSurfaceMap = SDL_GetWindowSurface(pWindow);
    if (pSurfaceMap == NULL) {
        return false;
    }
    return true;
}
void PrintFps()
{
    // Check if this frame is happening in the same second as last frame, if not, set the fps counter to 0 and print the frames that happened in the last second
    if (SDL_GetTicks64() / 1000 == iTimePassed / 1000) {
        iFps++;
    } else {
        SetupTextNumber(&sText_Fps, iFps);
        iFps = 0;
    }
    iTimePassed = SDL_GetTicks64();
}
void SetupStartScreen()
{
    // Init the starting cell
    sCellMap[0].cCell = cDarkYellow;
    sCellMap[0].rCell = {(SCREEN_WIDTH / 2 - (iCellWidth / 2)), 0, 10, 10};
    sCellMap[0].bActive = true;

    chInputNumber = 90;
    InitRuleset(chInputNumber);
    for (int i = 1; i < 80; i++) {
        SetupNextGeneration(10);
    }
}
void SetInput()
{
    std::cout << "Eingabe 0 - 255" << std::endl;
    std::cin >> chInputNumber;
    std::cout << "Eingabe: " << chInputNumber << std::endl;
    InitRuleset(chInputNumber);
}
void HandleKeys_S(SDL_Scancode *SKey)
{
    switch (*SKey) {
    case SDL_SCANCODE_ESCAPE:
        bEndBox_Start = true;
        bEndBox_Main = true;
        return;
    case SDL_SCANCODE_SPACE:
        bEndBox_Start = true;
        return;
    default:
        std::cout << "unhandled key pressed: " << *SKey << std::endl;
    }
}
void HandleKeys_M(SDL_Scancode *SKey)
{
    switch (*SKey) {
    case SDL_SCANCODE_ESCAPE:
        bEndBox_Main = true;
        return;
    case SDL_SCANCODE_LEFT:
        if (chInputNumber > 0) {
            ClearRule();
            chInputNumber--;
            SetupTextNumber(&sText_RuleValue, chInputNumber);
            InitRuleset(chInputNumber);
            for (int i = 1; i < iGenerationMax; i++) {
                SetupNextGeneration(iCellWidth);
            }
            FlushRects();
            SetupActiveMap();
            if (!SetupMapTexture()) {
                QuitBox();
            }
            // std::cout << "iCountTotalCells: " << iCountTotalCells << std::endl;
        }
        return;
    case SDL_SCANCODE_RIGHT:
        if (chInputNumber < 255) {
            ClearRule();
            chInputNumber++;
            SetupTextNumber(&sText_RuleValue, chInputNumber);
            InitRuleset(chInputNumber);
            for (int i = 1; i < iGenerationMax; i++) {
                SetupNextGeneration(iCellWidth);
            }
            FlushRects();
            SetupActiveMap();
            if (!SetupMapTexture()) {
                QuitBox();
            }
            // std::cout << "iCountTotalCells: " << iCountTotalCells << std::endl;
        }
        return;
    case SDL_SCANCODE_KP_PLUS:
        // std::cout << "Plus pushed: " << iCellWidth << std::endl;
        if (iCellWidth < 10) {
            ZoomCellsIn();
        }
        FlushRects();
        SetupActiveMap();
        if (!SetupMapTexture()) {
            QuitBox();
        }
        return;
    case SDL_SCANCODE_KP_MINUS:
        // std::cout << "Minus pushed: " << iCellWidth << std::endl;
        if (iCellWidth > 2) {
            ZoomCellsOut();
        }
        FlushRects();
        SetupActiveMap();
        if (!SetupMapTexture()) {
            QuitBox();
        }
        return;
    case SDL_SCANCODE_DOWN:
        /*if (iGenStart < 250) {
            ScrollCellsDown();
        }*/
        return;
    case SDL_SCANCODE_UP:
        /*if (iGenStart > 0) {
            ScrollCellsUp();
        }*/
        return;
    default:
        std::cout << "unhandled key pressed: " << *SKey << std::endl;
    }
}
void InitRuleset(int iNumber)
{
    // Convert Input to boolean
    for (int i = 7; i >= 0; i--) {
        chInputNumberBin[i] = (char)((iNumber % 2) + '0');
        iNumber = iNumber / 2;
    }
    // std::cout << "chInputNumberBin: " << chInputNumberBin << std::endl;
    for (int i = 0; i < 8; i++) {
        chInputNumberBin[7 - i] == '1' ? bConfigurationMap[i].bset = true : bConfigurationMap[i].bset = false;
        // std::cout << "Mapconfig: " << i << " - " << bConfigurationMap[i].bleft << bConfigurationMap[i].bmiddle << bConfigurationMap[i].bright << bConfigurationMap[i].bset << std::endl;
    }
}
void ClearNewFrame()
{
    SDL_SetRenderDrawColor(pRenderer, 0, 0, 0, 255);
    SDL_RenderClear(pRenderer);
}
void ClearRule()
{
    iGeneration = iCell = iCountTotalCells = 1;
    for (int i = 0; i < iTotalCells; i++) {
        sCellMap[i].bActive = false;
    }

    // Init the starting cell
    sCellMap[0].cCell = cDarkYellow;
    sCellMap[0].rCell = {(SCREEN_WIDTH / 2 - (iCellWidth / 2)), 0, iCellWidth, iCellWidth};
    sCellMap[0].bActive = true;
}
void FlushRects()
{
    // flush pointer array to rects
    for (int i = 0; i < iTotalCells; i++) {
        sCellRects[i] = {};
    }
}
void DrawCurrentFrame_S()
{
    if (iAlphaAn >= 255) {
        iAlphaAn = 255;
        bDecreaseAlpha = true;
    }
    if (iAlphaAn <= 50) {
        bDecreaseAlpha = false;
    }
    SDL_SetRenderDrawColor(pRenderer, cDarkYellow.r, cDarkYellow.g, cDarkYellow.b, cDarkYellow.a);
    for (int i = 0; i < iCountTotalCells; i++) {
        if (sCellMap[i].bActive) {
            SDL_RenderFillRect(pRenderer, &sCellMap[i].rCell);
        }
    }
    SDL_RenderCopy(pRenderer, sText_title.pTexture, NULL, &sText_title.rText);
    SDL_RenderCopy(pRenderer, sText_start.pTexture, NULL, &sText_start.rText);
    SDL_RenderCopy(pRenderer, sText_Fps.pTexture, NULL, &sText_Fps.rText);
    SDL_SetTextureAlphaMod(sText_start.pTexture, iAlphaAn);
    SDL_RenderPresent(pRenderer);
    if (!bDecreaseAlpha) {
        iAlphaAn += 5;
    } else {
        iAlphaAn -= 5;
    }
}
void DrawCurrentFrame_M()
{
    SDL_SetRenderDrawColor(pRenderer, cDarkYellow.r, cDarkYellow.g, cDarkYellow.b, cDarkYellow.a);
    // Rule texture
    SDL_RenderCopy(pRenderer, pTextureMap, NULL, NULL);
    // UI
    SDL_RenderCopy(pRenderer, sText_Rule.pTexture, NULL, &sText_Rule.rText);
    SDL_RenderCopy(pRenderer, sText_RuleValue.pTexture, NULL, &sText_RuleValue.rText);
    SDL_RenderCopy(pRenderer, sText_Keys.pTexture, NULL, &sText_Keys.rText);
    SDL_RenderCopy(pRenderer, sText_Zoom.pTexture, NULL, &sText_Zoom.rText);
    SDL_RenderCopy(pRenderer, sText_CellSize.pTexture, NULL, &sText_CellSize.rText);
    SDL_RenderCopy(pRenderer, sText_CellSizeValue.pTexture, NULL, &sText_CellSizeValue.rText);
    SDL_RenderCopy(pRenderer, sText_Fps.pTexture, NULL, &sText_Fps.rText);
    // Render the row numbers
    for (int i = 0; i < iRowNumbersMax; i++) {
        SDL_RenderCopy(pRenderer, psText_GenNum.at(i)->pTexture, NULL, &psText_GenNum.at(i)->rText);
    }
    SDL_RenderPresent(pRenderer);
}
void SetupNextGeneration(int iWidth)
{
    // Each generation increments 2
    // 0; 1; 4; 9; 16
    // iCell ist the current sum of cells in a row
    // The first two and last two cells of a Generation always deal with a similiar situation
    // If would not be efficient to test that condition for every cell

    int ixStart = sCellMap[0].rCell.x - iGeneration * iWidth;
    for (int i = 0; i < (iCell + 2); i++) {
        if (DecideCell(&sCellMap[iCountTotalCells], &i, &iCell)) {
            sCellMap[iCountTotalCells].cCell = cDarkYellow;
            sCellMap[iCountTotalCells].rCell = {(ixStart + i * iWidth), (iGeneration * iWidth), iWidth, iWidth};
            sCellMap[iCountTotalCells].bActive = true;
        } else {
            sCellMap[iCountTotalCells].bActive = false;
        }
        iCountTotalCells++;
    }

    iCell += 2;
    iGeneration++;
}
void SetupActiveMap()
{
    int y = 0;
    for (int i = 0; i < iCountTotalCells; i++) {
        if (sCellMap[i].bActive) {
            sCellRects[y] = sCellMap[i].rCell;
            y++;
        }
    }
    iCountRectsRendered = y;
}
bool SetupMapTexture()
{
    // Flush old surface, destroy old texture
    if (SDL_FillRect(pSurfaceMap, NULL, 0x000000) != 0) {
        std::cout << "SDL_FillRects Error: " << SDL_GetError() << std::endl;
    }
    SDL_DestroyTexture(pTextureMap);
    // Fill surface with the new rects
    if (SDL_FillRects(pSurfaceMap, sCellRects, iCountRectsRendered, SDL_MapRGBA(pSurfaceMap->format, cDarkYellow.r, cDarkYellow.g, cDarkYellow.b, cDarkYellow.a)) != 0) {
        std::cout << "SDL_FillRects Error: " << SDL_GetError() << std::endl;
        return false;
    }
    // Get a texture of the rect surface
    pTextureMap = SDL_CreateTextureFromSurface(pRenderer, pSurfaceMap);
    if (pTextureMap == NULL) {
        std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
        return false;
    }
    return true;
}
bool DecideCell(sCell *pCell, int *pRowindex, int *iCell)
{
    /* Start: middle and left are always white/false
    End: middle and right are always white/false
    This could be handled more efficient

    1. Get 3 states
    2. Sum the decimal value to use to lookup the matching rule
    3. Lookup if the rule for that case is true or not and return */

    int ileft, imiddle, iright, idec;
    ileft = imiddle = iright = idec = 0;

    ileft = *iCell + 2;
    imiddle = *iCell + 1;
    iright = *iCell;
    sCellMap[iCountTotalCells - ileft].bActive && !(*pRowindex <= 1) ? idec += 4 : idec = 0;
    sCellMap[iCountTotalCells - imiddle].bActive && !(*pRowindex == 0) && !(*pRowindex == *iCell + 1) ? idec += 2 : idec += 0;
    sCellMap[iCountTotalCells - iright].bActive && !(*pRowindex >= *iCell) ? idec += 1 : idec += 0;

    // Lookup the fitting rule and its status setted by the given number
    if (bConfigurationMap[idec].bset) {
        return true;
    } else {
        return false;
    }
}
void ZoomCellsIn()
{
    iCellWidth++;
    sCellMap[0].rCell = {(SCREEN_WIDTH / 2 - (iCellWidth / 2)), 0, iCellWidth, iCellWidth};
    ZoomCells(iCellWidth);
}
void ZoomCellsOut()
{
    iCellWidth--;
    sCellMap[0].rCell = {(SCREEN_WIDTH / 2 - (iCellWidth / 2)), 0, iCellWidth, iCellWidth};
    ZoomCells(iCellWidth);
}
void ZoomCells(int iWidth)
{
    iGeneration = iCell = iCountTotalCells = 1;
    // Rescale the view to the number of generations fitting into the screen with the new cell width iWidth
    iGenStop = (SCREEN_HEIGHT / iWidth);
    for (int i = 1; i < iGenStop; i++) {
        int ixStart = sCellMap[0].rCell.x - iGeneration * iWidth;
        for (int i = 0; i < (iCell + 2); i++) {
            sCellMap[iCountTotalCells].rCell = {(ixStart + i * iWidth), (iGeneration * iWidth), iWidth, iWidth};
            iCountTotalCells++;
        }

        iCell += 2;
        iGeneration++;
    }
    iZoomedCells = iCountTotalCells;
    AdjustTextNumberRow(iWidth);
    SetupTextNumber(&sText_CellSizeValue, iWidth);
}
void QuitBox()
{
    delete[] sCellMap;
    delete[] sCellRects;
    // delete[] psCellRects;
    for (int i = 0; i < (int)sBoxes.size(); i++) {
        SDL_DestroyTexture(sBoxes[i]->pTexture);
    }
    for (int i = 0; i < (int)sText_GenNum.size(); i++) {
        SDL_DestroyTexture(sText_GenNum[i].pTexture);
    }
    SDL_FreeSurface(pSurfaceMap);
    SDL_DestroyTexture(pTextureMap);
    SDL_DestroyRenderer(pRenderer);
    SDL_DestroyWindow(pWindow);
    pRenderer = nullptr;
    pWindow = nullptr;
    pSurfaceMap = nullptr;
    TTF_Quit();
    SDL_Quit();
    std::cout << "QuitBox executed" << std::endl;
}